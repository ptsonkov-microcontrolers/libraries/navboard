#include <Arduino.h>
#include <NavBoard.h>

/*
  Deploy this example file to print current voltage drops and button assignment
  Using printed voltage drops, do a fine tuning of board setup.

  Parameters of function as follows:
  param1 - Analog pin to be read (A0-A7)
  param2 - button1 voltage drop (CANCEL/BACK)
  param3 - button2 voltage drop (LEFT)
  param4 - button3 voltage drop (UP)
  param5 - button4 voltage drop (DOWN)
  param6 - button5 voltage drop (RIGHT)
  param7 - button6 voltage drop (SELECT/ENTER)
*/

byte analogPin = A0;

// NavBoard board(analogPin, 120, 150, 180, 210, 240, 280);
NavBoard board(analogPin, 90, 130, 160, 190, 220, 250);

void testPrint();

void setup()
{
	Serial.begin(9600);
}

void loop() {
	testPrint();
  delay(1000);
}

void testPrint() {
	int button = board.setButton();
	Serial.print("Button assignment: ");
  // Debugging voltage drop at analog pin to do finetuning of initialization
  Serial.print(" (voltage drop ");
	Serial.print(analogRead(analogPin));
  Serial.print(")");
	Serial.println();
	Serial.print("button ");
	switch (button) {
	case 0:
		Serial.print(" ------");
		break;
	case 1:
		Serial.print(" (BACK)");
		break;
	case 2:
		Serial.print(" (LEFT)");
		break;
	case 3:
		Serial.print(" (UP)");
		break;
	case 4:
		Serial.print(" (DOWN)");
		break;
	case 5:
		Serial.print(" (RIGHT)");
		break;
	case 6:
		Serial.print(" (SELECT)");
		break;
	}
	Serial.println();
	Serial.println();
}
