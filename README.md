# NavBoard
NavBoard library and instructions to create and use navigation board.\
Board consists of 6 buttons connected to one analog input on Arduino board. By design and board layout, buttons are:
- CANCEL/BACK
- RIGHT
- UP
- DOWN
- LEFT
- SELECT/ENTER

but they can be used as anything else depend on the project.

## The library
Library depends on voltage drop using different count of resistors.\
Voltage drops depend on original design using 1.5K resistors, when use different resistors, need to change and voltage drops (pass it as init parameters).\
By default it is set to use analog input A0. Depend on the project, it also can be changed (as init parameter)

## The board
Board is created with Perfboard (16x16 holes), 6 tactile switches (6mm) and 6 resistors (1.5K). You can find detailed layout, schema and PCB in the "extras" directory.
Example project can be used to print button assignments and current voltage drops in order to fine tune the board initialization
