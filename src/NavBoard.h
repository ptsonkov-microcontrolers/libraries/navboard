#ifndef NAVBOARD_H
#define NAVBOARD_H

#include <Arduino.h>

class NavBoard {

  private:
    byte aPin;
    int volt1;
    int volt2;
    int volt3;
    int volt4;
    int volt5;
    int volt6;

  public:
    // Setup analog pin and voltage drops
    NavBoard(byte aPin, int volt1, int volt2, int volt3, int volt4, int volt5, int volt6);

    // Assign voltage drop to button
    int setButton();
};

#endif
