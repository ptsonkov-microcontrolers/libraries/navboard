#include "NavBoard.h"

NavBoard::NavBoard(byte aPin, int volt1, int volt2, int volt3, int volt4, int volt5, int volt6) {
	this->aPin = aPin;
	this->volt1 = volt1;
	this->volt2 = volt2;
	this->volt3 = volt3;
	this->volt4 = volt4;
	this->volt5 = volt5;
	this->volt6 = volt6;

  // Initialize analog pin
  pinMode(aPin, INPUT_PULLUP);
}

int NavBoard::setButton() {
  int vDrop = analogRead(aPin);
	int buttton = 0;
	if (vDrop < volt1) {
		buttton = 1; // click Back
	} else if (vDrop < volt2) {
		buttton = 2; // click Left
	} else if (vDrop < volt3) {
		buttton = 3; // click Up
	} else if (vDrop < volt4) {
		buttton = 4; // click Down
	} else if (vDrop < volt5) {
		buttton = 5; // click Right
	} else if (vDrop < volt6) {
		buttton = 6; // click Select
	}
	return buttton;
}
